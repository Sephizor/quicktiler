﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace QuickTiler
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Screen[] monitors = Screen.AllScreens;
            List<Rectangle> resolutions = new List<Rectangle>();

            foreach (Screen r in monitors)
            {
                resolutions.Add(r.Bounds);
            }

            foreach (Screen s in monitors)
            {
                Application.Run(new QuickTilerApp(resolutions));
            }
        }
    }
}
