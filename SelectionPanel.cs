﻿using System.Drawing;
using System.Windows.Forms;

namespace QuickTiler
{
    class SelectionPanel : Panel
    {
        private Rectangle mouseSelectRectangle;
        private int rows;
        private int cols;
        private int topAnchor;
        private int leftAnchor;
        private System.Collections.Generic.List<Rectangle> miniTiles;
        
        public SelectionPanel()
        {
            miniTiles = new System.Collections.Generic.List<Rectangle>();

            this.BackColor = Color.FromArgb(0, 20, 20, 20);
            this.DoubleBuffered = true;
            rows = 6;
            cols = 6;
            CreateMiniTiles();
        }

        private void CreateMiniTiles()
        {
            Graphics g = this.CreateGraphics();
            int[] margin =
            {
                2,  // [0] X
                2   // [1] Y
            };
            int miniTileWidth = this.Width / cols;
            int miniTileHeight = this.Height / rows;

            Pen pen = new Pen(Color.Blue, 2);
            for (int i = 0; i < cols; i++)
            {
                for (int j = 0; j < rows; j++)
                {
                    int x = (miniTileWidth * i) + margin[0];
                    int y = (miniTileHeight * j) + margin[1];

                    miniTiles.Add(new Rectangle(
                        x,
                        y,
                        miniTileWidth,
                        miniTileHeight
                    ));
                }
            }
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            mouseSelectRectangle = new Rectangle(e.X, e.Y, 0, 0);
            leftAnchor = mouseSelectRectangle.Left;
            topAnchor = mouseSelectRectangle.Top;
            this.Invalidate();
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                int newX = mouseSelectRectangle.Left;
                int newY = mouseSelectRectangle.Top;
                int width = 0;
                int height = 0;

                if (e.X <= leftAnchor)
                {
                    newX = e.X;
                    width = leftAnchor - e.X;
                }
                else
                {
                    width = e.X - leftAnchor;
                }

                if (e.Y <= topAnchor)
                {
                    newY = e.Y;
                    height = topAnchor - e.Y;
                }
                else
                {
                    height = e.Y - topAnchor;
                }

                mouseSelectRectangle = new Rectangle(
                    newX,
                    newY,
                    width,
                    height
                );
                this.Invalidate();
            }
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            mouseSelectRectangle = new Rectangle();
            this.Invalidate();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            using (Pen pen = new Pen(Color.Cyan, 1.5f))
            {
                e.Graphics.DrawRectangle(pen, mouseSelectRectangle);
            }
        }
    }
}
