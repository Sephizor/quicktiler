﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace QuickTiler
{
    public partial class QuickTilerForm : Form
    {
        private int rows;
        private int cols;
        private int tileWidth;
        private int tileHeight;
        private Rectangle screen;

        public QuickTilerForm(Rectangle screen)
        {
            InitializeComponent();
            rows = 6;
            cols = 6;
            this.screen = screen;
            this.DoubleBuffered = true;
            tileWidth = this.screen.Width / rows;
            tileHeight = this.screen.Height / cols;
        }

        public void SetPosition(Rectangle loc)
        {
            Location = new Point(loc.X - (Width/2), loc.Y - (Height/2));
        }

        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
        (
            int nLeftRect, // x-coordinate of upper-left corner
            int nTopRect, // y-coordinate of upper-left corner
            int nRightRect, // x-coordinate of lower-right corner
            int nBottomRect, // y-coordinate of lower-right corner
            int nWidthEllipse, // height of ellipse
            int nHeightEllipse // width of ellipse
         );

        [DllImport("gdi32.dll", EntryPoint = "DeleteObject")]
        private static extern bool DeleteObject(System.IntPtr hObject);

        [DllImport("user32.dll")]
        static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll")]
        static extern int GetWindowText(IntPtr hwnd, StringBuilder text, int count);

        private void QuickTilerForm_Load(object sender, EventArgs e)
        {
            Visible = false;

            ShowInTaskbar = false;

            this.FormBorderStyle = FormBorderStyle.None;
            this.BackColor = Color.FromArgb(20, 20, 20);
            this.Opacity = 0.85;
            this.TopMost = true;
            IntPtr border = CreateRoundRectRgn(0, 0, Width, Height, 20, 20);
            try {
                Region = System.Drawing.Region.FromHrgn(border);
            }
            finally
            {
                DeleteObject(border);
            }
        }

        private void exitItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void settingsButton_Click(object sender, EventArgs e)
        {
            //Nothing yet!
        }
    }
}
