﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace QuickTiler
{
    public partial class QuickTilerApp : Form
    {
        private MenuItem openItem;
        private MenuItem exitItem;
        private bool showing;
        private QuickTilerForm[] forms;
        private Rectangle[] formLocations;

        public QuickTilerApp(List<Rectangle> screens)
        {
            InitializeComponent();
            forms = new QuickTilerForm[screens.Count];
            formLocations = new Rectangle[screens.Count];
            for (int i = 0; i < screens.Count; i++)
            {
                formLocations[i] = new Rectangle(
                    (screens[i].Width / 2) + screens[i].X, (screens[i].Height / 2) + screens[i].Y,
                    400, 400
                );
                forms[i] = new QuickTilerForm(screens[i]);
                forms[i].VisibleChanged += handleClose;
                forms[i].Width = 400;
                forms[i].Height = 400;
            }
        }

        private void handleClose(object sender, EventArgs e)
        {
            if (!showing)
            {
                return;
            }
            showing = false;
            foreach (QuickTilerForm f in forms)
            {
                f.Hide();
            }
        }

        private void ShowForms(object sender, EventArgs e)
        {
            if (showing)
            {
                return;
            }

            showing = true;
            int index = 0;
            foreach (QuickTilerForm f in forms)
            {
                f.Show();
                f.SetPosition(formLocations[index]);
                index++;
            }
        }

        private void QuickTilerApp_Load(object sender, EventArgs e)
        {
            showing = false;
            Visible = false;
            
            ShowInTaskbar = false;
            ShowIcon = true;

            openItem = new MenuItem();
            openItem.Text = "Open";
            openItem.DefaultItem = true;
            openItem.Click += ShowForms;

            exitItem = new MenuItem();
            exitItem.Text = "Exit";
            exitItem.Click += exitItem_Click;

            quickTilerTray.ContextMenu = new ContextMenu(new MenuItem[] { openItem, exitItem });
            quickTilerTray.DoubleClick += ShowForms;
        }

        private void exitItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void quickTilerTray_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (showing)
            {
                handleClose(sender, e);
                return;
            }
            ShowForms(sender, e);
        }
    }
}
